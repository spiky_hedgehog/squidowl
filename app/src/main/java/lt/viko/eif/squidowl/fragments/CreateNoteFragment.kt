package lt.viko.eif.squidowl.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_create_note.*
import kotlinx.coroutines.*
import lt.viko.eif.squidowl.R
import lt.viko.eif.squidowl.models.Note
import lt.viko.eif.squidowl.utils.toast
import lt.viko.eif.squidowl.viewmodels.NotebookViewModel
import java.util.*


class CreateNoteFragment : Fragment(R.layout.fragment_create_note) {

    private lateinit var notebookViewModel: NotebookViewModel

    private lateinit var filePath: Uri

    private lateinit var directLink: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        newOutlinedSelectImageButton.setOnClickListener {
            startFileChooser()
        }

        notebookViewModel = ViewModelProvider(this).get(NotebookViewModel::class.java)

        newOutlinedSaveButton.setOnClickListener {

            val newNoteTitleEntered = newNoteTitle.editText?.text.toString()
            val newNoteBodyEntered = newNoteBody.editText?.text.toString()
            directLink = "https://clickup.com/blog/wp-content/uploads/2020/01/note-taking.png"

            if (newNoteTitleEntered != "" && newNoteBodyEntered != "") {
                if (this::filePath.isInitialized) {
                    GlobalScope.launch {
                        suspend {
                            val imageName = Random().nextLong()
                            val imageRef =
                                FirebaseStorage.getInstance().reference.child("images/$imageName.jpg")

                            imageRef.putFile(filePath).continueWithTask { task ->
                                if (!task.isSuccessful) {
                                    task.exception?.let {
                                        throw it
                                    }
                                }
                                imageRef.downloadUrl
                            }
                                .addOnCompleteListener { task ->
                                    val url = task.result
                                    directLink = url.toString()
                                }
                                .addOnFailureListener { p0 ->
                                    context?.toast(p0.message.toString())
                                }
                            delay(5000)
                            withContext(Dispatchers.Main) {
                                notebookViewModel.addNote(
                                    Note(
                                        Random().nextLong(),
                                        newNoteTitleEntered,
                                        newNoteBodyEntered,
                                        directLink
                                    )
                                )
                                activity?.toast(getString(R.string.saved))
                                val action =
                                    CreateNoteFragmentDirections.actionCreateNoteFragmentToHomeFragment(
                                        null
                                    )
                                findNavController().navigate(action)
                            }
                        }.invoke()
                    }
                } else {
                    notebookViewModel.addNote(
                        Note(
                            Random().nextLong(),
                            newNoteTitleEntered,
                            newNoteBodyEntered,
                            directLink
                        )
                    )
                    activity?.toast(getString(R.string.saved))
                    val action =
                        CreateNoteFragmentDirections.actionCreateNoteFragmentToHomeFragment(
                            null
                        )
                    findNavController().navigate(action)
                }
            } else {
                activity?.toast(getString(R.string.empty_fields_warning))
            }
        }
    }

    private fun startFileChooser() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select image"), 111)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            filePath = data.data!!
            val imageUri = filePath
            imageView.setImageURI(imageUri)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}