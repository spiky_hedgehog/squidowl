package lt.viko.eif.squidowl.repositories

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import lt.viko.eif.squidowl.models.Note

object NotebookRepository {
    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser?.uid

    fun addNote(note: Note): Task<Void> {
        val document =
            db.collection("notebooks").document(user!!).collection("user_notes")
                .document("${note.id}")
        return document.set(note)
    }

    fun fetchUserNotes(): CollectionReference {
        return db.collection("notebooks/${user!!}/user_notes")
    }

    fun deleteUserNote(id: Long): Task<Void> {
        val document = db.collection("notebooks/${user!!}/user_notes").document("$id")
        return document.delete()
    }

}