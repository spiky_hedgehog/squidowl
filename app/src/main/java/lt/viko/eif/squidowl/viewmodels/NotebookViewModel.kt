package lt.viko.eif.squidowl.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import lt.viko.eif.squidowl.models.Note
import lt.viko.eif.squidowl.repositories.NotebookRepository

const val TAG = "NOTEBOOK'SVIEWMODEL"

class NotebookViewModel : ViewModel() {

    private val repository = NotebookRepository
    private val notebook: MutableLiveData<List<Note>> = MutableLiveData()

    fun addNote(note: Note) {
        repository.addNote(note).addOnSuccessListener {
            Log.w(TAG, "Note added.")
        }.addOnFailureListener {
            Log.w(TAG, "Failed to add note: $it")
        }
    }

    fun fetchUserNotes(): LiveData<List<Note>> {

        repository.fetchUserNotes().addSnapshotListener { value, _ ->
            val tempList: MutableList<Note> = mutableListOf()
            value!!.forEach { document ->
                val note = document.toObject(Note::class.java)
                tempList.add(note)
            }
            notebook.value = tempList
        }
        return notebook
    }

    fun deleteUserNote(id: Long) {
        repository.deleteUserNote(id).addOnFailureListener {
            Log.w(TAG, "Failed to remove: $it")
        }
    }

}