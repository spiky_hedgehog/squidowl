package lt.viko.eif.squidowl.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Note(
    val id: Long = 0L,
    val title: String = "",
    var body: String = "",
    var imageURl: String = ""
) : Parcelable