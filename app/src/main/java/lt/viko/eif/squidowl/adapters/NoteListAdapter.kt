package lt.viko.eif.squidowl.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.View.OnClickListener
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_note_layout.view.*
import lt.viko.eif.squidowl.R
import lt.viko.eif.squidowl.models.Note

class NoteListAdapter(private val interaction: Interaction? = null) :
    ListAdapter<Note, NoteListAdapter.NoteViewHolder>(NoteDC()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = NoteViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.list_note_layout, parent, false), interaction
    )

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) =
        holder.bind(getItem(position))

    fun swapData(data: List<Note>) {
        submitList(data.toMutableList())
    }

    inner class NoteViewHolder(
        itemView: View,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView), OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {

            if (adapterPosition == RecyclerView.NO_POSITION) return

            val clicked = getItem(adapterPosition)

            interaction?.clickNote(clicked)
        }

        fun bind(item: Note) = with(itemView) {
            note_title.text = item.title
            note_body.text = item.body
            Picasso.get().load(item.imageURl).into(note_image)
        }
    }

    interface Interaction {
        fun clickNote(item: Note)
    }

    private class NoteDC : DiffUtil.ItemCallback<Note>() {
        override fun areItemsTheSame(
            oldItem: Note,
            newItem: Note
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: Note,
            newItem: Note
        ) = oldItem == newItem
    }
}