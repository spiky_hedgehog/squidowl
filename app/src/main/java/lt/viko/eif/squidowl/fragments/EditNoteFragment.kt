package lt.viko.eif.squidowl.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_edit_note.*
import lt.viko.eif.squidowl.R
import lt.viko.eif.squidowl.models.Note
import lt.viko.eif.squidowl.utils.toast
import lt.viko.eif.squidowl.viewmodels.NotebookViewModel

class EditNoteFragment : Fragment(R.layout.fragment_edit_note) {

    private val args: NoteOverviewFragmentArgs by navArgs()
    private lateinit var notebookViewModel: NotebookViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        noteTitle.editText?.setText(args.note?.title)
        noteBody.editText?.setText(args.note?.body)

        notebookViewModel = ViewModelProvider(this).get(NotebookViewModel::class.java)

        outlinedSaveButton.setOnClickListener {

            val noteTitleEntered = noteTitle.editText?.text.toString()
            val noteBodyEntered = noteBody.editText?.text.toString()
            if (noteTitleEntered != "" && noteBodyEntered != "") {

                args.note?.id?.let { it1 ->
                    args.note?.imageURl?.let { it2 ->
                        Note(
                            it1,
                            noteTitleEntered,
                            noteBodyEntered,
                            it2
                        )
                    }
                }?.let { it2 ->
                    notebookViewModel.addNote(
                        it2
                    )
                }
                activity?.toast(getString(R.string.updated))
                val action = EditNoteFragmentDirections.actionEditNoteFragmentToHomeFragment(null)
                findNavController().navigate(action)
            } else {
                activity?.toast(getString(R.string.empty_fields_warning))
            }

        }

    }

}