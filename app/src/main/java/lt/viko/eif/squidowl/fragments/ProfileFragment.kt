package lt.viko.eif.squidowl.fragments

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.fragment.app.Fragment
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import lt.viko.eif.squidowl.R
import lt.viko.eif.squidowl.activities.LoginActivity
import lt.viko.eif.squidowl.utils.toast

class ProfileFragment : Fragment() {

    private lateinit var logOutButton: Button
    private lateinit var nukeAccountButton: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        logOutButton = view.findViewById(R.id.signOutButton)
        nukeAccountButton = view.findViewById(R.id.deleteAccountButton)

        val firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        user?.let {
            val name = user.displayName
            val email = user.email
            val photoUrl = user.photoUrl
            //val uid = user.uid

            val ogPieceOfUrl = "s96-c"
            val highResPieceOfUrl = "s400-c"
            if (photoUrl != null) {
                val photoPath = photoUrl.toString()
                val highResFullUrl: String = photoPath.replace(ogPieceOfUrl, highResPieceOfUrl)
                Picasso.get().load(highResFullUrl).into(imageViewProfilePic)

            }
            editTextEmailAddress.setText(email)
            editTextDisplayName.setText(name)
        }


        logOutButton.setOnClickListener {
            activity?.toast(getString(R.string.see_you_toast) + user?.displayName)
            context?.let { it1 ->
                AuthUI.getInstance()
                    .signOut(it1)
                    .addOnCompleteListener {
                        startActivity(Intent(activity, LoginActivity::class.java))
                        activity?.let { it2 -> finishAffinity(it2) }
                    }
                playSound(R.raw.door_locking)
            }
        }

        nukeAccountButton.setOnClickListener {
            activity?.toast(getString(R.string.bye_toast) + user?.displayName)
            context?.let { it1 ->
                AuthUI.getInstance()
                    .delete(it1)
                    .addOnCompleteListener {
                        startActivity(Intent(activity, LoginActivity::class.java))
                        activity?.let { it2 -> finishAffinity(it2) }
                    }
                playSound(R.raw.whoop)
            }
        }

    }

    private fun playSound(sound: Int) {
        val mediaPlayer = MediaPlayer.create(context, sound)
        mediaPlayer.start()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }


}
