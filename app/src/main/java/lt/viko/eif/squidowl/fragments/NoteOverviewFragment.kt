package lt.viko.eif.squidowl.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_note_overview.*
import lt.viko.eif.squidowl.R
import lt.viko.eif.squidowl.models.Note
import lt.viko.eif.squidowl.utils.toast
import lt.viko.eif.squidowl.viewmodels.NotebookViewModel

class NoteOverviewFragment : Fragment(R.layout.fragment_note_overview) {

    private val args: HomeFragmentArgs by navArgs()
    private lateinit var notebookViewModel: NotebookViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        notebookViewModel = ViewModelProvider(this).get(NotebookViewModel::class.java)

        val id = args.note?.id
        val title = args.note?.title
        val body = args.note?.body
        val imageURl = args.note?.imageURl

        noteTitleInOverview.text = title
        bodyOnCard.text = body
        Picasso.get().load(imageURl).into(noteImageInOverview)


        deleteButton.setOnClickListener {
            if (id != null) {
                notebookViewModel.deleteUserNote(id)
            }
            activity?.toast(getString(R.string.note_deleted))
            val action =
                NoteOverviewFragmentDirections.actionNoteOverviewFragmentToHomeFragment(null)
            findNavController().navigate(action)
        }


        editButton.setOnClickListener {

            if (id!= null && title!=null && body != null && imageURl!= null) {
                val currentNote = Note(
                    id,
                    title,
                    body,
                    imageURl
                )
                val action =
                    NoteOverviewFragmentDirections.actionNoteOverviewFragmentToEditNoteFragment(currentNote)
                findNavController().navigate(action)
            }

        }

    }

}