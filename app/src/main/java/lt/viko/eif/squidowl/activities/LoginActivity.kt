package lt.viko.eif.squidowl.activities

import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import lt.viko.eif.squidowl.BuildConfig
import lt.viko.eif.squidowl.R
import lt.viko.eif.squidowl.utils.toast

const val MY_REQUEST_CODE = 586

class LoginActivity : AppCompatActivity() {

    private lateinit var providers: List<AuthUI.IdpConfig>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        providers = listOf(
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.TwitterBuilder().build()
        )
        showLoginOptions()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                playLoginSound()
                startActivity(intent)
                finish()
            } else {
                toast(getString(R.string.error_toast) + response!!.error!!.message)
            }

        }
    }

    private fun playLoginSound() {
        val mediaPlayer = MediaPlayer.create(this, R.raw.door_unlock)
        mediaPlayer.start()
    }

    private fun showLoginOptions() {
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTosAndPrivacyPolicyUrls(
                    "https://stanul.com/tos.html",
                    "https://stanul.com/privacypolicy.html"
                )
                .setTheme(R.style.AppTheme)
                .setIsSmartLockEnabled(!BuildConfig.DEBUG /* credentials */, true /* hints */)
                .build(),
            MY_REQUEST_CODE
        )
    }
}