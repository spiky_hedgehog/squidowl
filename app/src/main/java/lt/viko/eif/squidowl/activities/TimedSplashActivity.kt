package lt.viko.eif.squidowl.activities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowInsets
import android.view.WindowManager
import com.google.firebase.auth.FirebaseAuth
import lt.viko.eif.squidowl.R
import java.util.*
import kotlin.concurrent.schedule

class TimedSplashActivity : AppCompatActivity() {

    private var timer = Timer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_timed_splash)

        @Suppress("DEPRECATION")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }

        val destination: Class<*> = if (FirebaseAuth.getInstance().currentUser != null) {
            MainActivity::class.java
        } else {
            LoginActivity::class.java
        }

        timer.schedule(2000) {
            val intent = Intent(this@TimedSplashActivity, destination)
            startActivity(intent)
            finish()
        }


    }

    override fun onPause() {
        timer.cancel()
        super.onPause()
    }
}