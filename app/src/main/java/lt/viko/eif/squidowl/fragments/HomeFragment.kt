package lt.viko.eif.squidowl.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_home.*
import lt.viko.eif.squidowl.R
import lt.viko.eif.squidowl.adapters.NoteListAdapter
import lt.viko.eif.squidowl.models.Note
import lt.viko.eif.squidowl.viewmodels.NotebookViewModel

class HomeFragment : Fragment(R.layout.fragment_home), NoteListAdapter.Interaction {

    private lateinit var notebookViewModel: NotebookViewModel
    private val user = FirebaseAuth.getInstance().currentUser

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        notebookViewModel = ViewModelProvider(this).get(NotebookViewModel::class.java)

        val noteListAdapter: NoteListAdapter by lazy { NoteListAdapter(this) }

        if (user != null) {
            notebookViewModel.fetchUserNotes()
                .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                    noteListAdapter.swapData(it)
                })
        }

        note_recycleView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = noteListAdapter
        }

    }

    override fun clickNote(item: Note) {
        val action = HomeFragmentDirections.actionHomeFragmentToNoteOverviewFragment(item)
        findNavController().navigate(action)
    }


}